#include <iostream>
#include <string>
using namespace std;

class MyBigNumber {
    public: string sum(string num1, string num2)
    {
        string result = ""; 

        int carry = 0; 

    
        while (num1.length() < num2.length())
        {
            num1 = "0" + num1;
        }

        while (num2.length() < num1.length())
        {
            num2 = "0" + num2;
        }

      
        for (int i = num1.length() - 1; i >= 0; i--)
        {
            int digit1 = num1[i] - '0';
            int digit2 = num2[i] - '0';
            int sum = digit1 + digit2 + carry;

          
            if (sum >= 10)
            {
                carry = 1;
                sum -= 10;
            }
            else
            {
                carry = 0;
            }

          
            result = (char)(sum + '0') + result;
        }

     
        if (carry > 0)
        {
            result = "1" + result;
        }

        return result;
    }
};

int main()
{
    MyBigNumber number;

    string num1, num2;
    cin >> num1 >> num2;

    string result = number.sum(num1, num2);
    cout << "Tong hai so la: " << result << endl;

    return 0;
}
